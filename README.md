# wiki-tools

Simple scripts to perform various tasks on a Dokuwiki instance.

## Scripts

### `wiki-pages-per-namespace.sh`

Very simple stats that show pages per namespace. Example:

```text
$ wiki-pages-par-namespace.sh 10
153  tech/
102  product/
46   tools/
43   procedures/
41   paperwork/
```

## `wiki-search`

The wiki holds your infrastructure documentation. Suddently, your
infrastructure goes kaboom. Infortunately, emergency instructions are stored
into the wiki, which is down.

Lucky you, you have  a local copy! Let's dig it!

This is a simple wrapper around `grep(1)` (implicit `--recursive` switch):

```sh
# What's this server again?
wiki-search hostname

# Where do we have credentials around?
wiki-search -li "password:"

# How much do we know this domain.tld and its subdomains?
# (greedy regex)
wiki-search -E  -o -h 'https?://(.*\.)?example\.com' | sort -u
```
