#!/bin/bash

# Ubercomplex configuration
WIKIROOT='/srv/data/web/vhosts/wiki.bsf-intranet.org/htdocs/data/pages'

# First arg is how long should it be. Default: top10
LIMIT=${1:-10}

# GOTO Wonderland
cd $WIKIROOT|| {
    >&2 echo "Error: ${WIKIROOT}: can't cd in."
    exit 2  # ENOENT
}

# I know shell-fu!
for i in */ ; do
    echo "$( find "$i" -type f -name '*.txt' | wc -l ) $i"
done \
    | sort -rn \
    | head -n "$LIMIT" \
    | column -t
